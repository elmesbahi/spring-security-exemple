<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="public/css/style.css">
<meta charset="ISO-8859-1">
<title>liste des produits</title>
</head>
<body>
<a href="admin/ajout.html">ajout produit</a>
<hr>
<table>
	<tr><td>id</td><td>label</td><td>prix</td></tr>
	<c:forEach items="${produits }" var="prod" varStatus="index">
		<tr class="${index.count%2==0?'pair':'impair'}">
			<td><img src="/produit/${prod.id }/image/img.${prod.extension}" /></td>
			<td>${prod.id }</td>
			<td>${prod.label }</td>
			<td>${prod.prix }</td>
		</tr>
	</c:forEach>
</table>

</body>
</html>