<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>ajout produit</title>
</head>
<body>
Ajout produit
<form action="ajout.html" method="post" enctype="multipart/form-data" autocomplete="off">
	<fieldset>
		<legend>label :</legend>
 		<input name="label" type="text">
	</fieldset>
	<fieldset>
		<legend>prix :</legend>
 		<input name="prix" type="number" step="0.01">
	</fieldset>
	<fieldset>
		<legend>image :</legend>
 		<input name="image" type="file">
	</fieldset>
	<button>valider</button>
</form>

</body>
</html>