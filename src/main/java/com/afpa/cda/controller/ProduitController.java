package com.afpa.cda.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.cda.dto.DownloadFileDto;
import com.afpa.cda.service.IProduitService;

@Controller
public class ProduitController {
	@Autowired
	private IProduitService produitService;

	@RequestMapping(path = { "/", "list.html" }, method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView accueil(ModelAndView mv) {

		mv.addObject("produits", this.produitService.findAll());
		mv.addObject("nom", "Clement");
		mv.setViewName("list");

		return mv;
	}

	@GetMapping("/produit/{id}/image/img.*")
	public ResponseEntity<Object> downloadImage(@PathVariable int id) {
		
		DownloadFileDto image = null;
		ResponseEntity<Object> responseEntity =null;
		HttpHeaders headers = new HttpHeaders();
		try {
			image = this.produitService.getImage(id);
			headers.setContentLength(image.getLength());
			headers.setCacheControl(CacheControl.noCache().getHeaderValue());
			responseEntity = new ResponseEntity<>(image.getInputStream(), headers, HttpStatus.OK);
		} catch (IOException e) {
			responseEntity = new ResponseEntity<>("", headers, HttpStatus.NO_CONTENT);
		}

		return responseEntity;
	}

}
