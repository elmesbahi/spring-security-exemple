package com.afpa.cda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AccueilController {
	
	@GetMapping(path = {"login.html"})
	public ModelAndView login1(ModelAndView mv) {
		mv.setViewName("login");
		return mv;
	}
	
}
