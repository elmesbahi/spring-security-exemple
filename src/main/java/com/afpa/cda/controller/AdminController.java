package com.afpa.cda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.cda.dto.ProduitDto;
import com.afpa.cda.service.IProduitService;

@Controller
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	private IProduitService produitService;
	
	@GetMapping("/ajout.html")
	public ModelAndView initAjoutPersonne(ModelAndView mv) {
		mv.setViewName("ajout");
		return mv;
	}
	
	@PostMapping("/ajout.html")
	public ModelAndView ajoutPersonne(
			@RequestParam String label,
			@RequestParam float prix,
			@RequestParam(required = false) MultipartFile image,
			ModelAndView mv) {
		
		this.produitService.add(
				ProduitDto.builder()
					.label(label)
					.prix(prix)
					.image(image)
					.build());
		
		mv.setViewName("redirect:/list.html");
		return mv;
	}
	
}
