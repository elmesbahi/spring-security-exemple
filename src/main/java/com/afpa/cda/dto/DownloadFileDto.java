package com.afpa.cda.dto;

import org.springframework.core.io.InputStreamResource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DownloadFileDto {
	private long length;
	private InputStreamResource inputStream;
	private String contentType;
}
