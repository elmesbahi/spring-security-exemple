package com.afpa.cda.service;

import java.io.IOException;
import java.util.List;

import com.afpa.cda.dto.DownloadFileDto;
import com.afpa.cda.dto.ProduitDto;

public interface IProduitService {

	List<ProduitDto> findAll();

	void add(ProduitDto produitDto);

	DownloadFileDto getImage(int prodId) throws IOException;

}
