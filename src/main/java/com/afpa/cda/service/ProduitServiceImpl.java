package com.afpa.cda.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.afpa.cda.dao.ProduitDao;
import com.afpa.cda.dto.DownloadFileDto;
import com.afpa.cda.dto.ProduitDto;
import com.afpa.cda.entity.Produit;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProduitServiceImpl implements IProduitService {

	@Autowired
	private ProduitDao produitDao;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Value( "${cda.upload.folder}" )
	private String uploadFolderPath;
	
	@PostConstruct
	public void init() {
		File uploadFolder = new File(this.uploadFolderPath);
		if(! uploadFolder.exists()) {
			log.info("creation : "+uploadFolder.getAbsolutePath());
			uploadFolder.mkdirs();
		} else if(uploadFolder.isFile()) {
			throw new RuntimeException("attention il y a un File de type fichier qui a le nom "+uploadFolder.getAbsolutePath());
		}
	}
	
	@Override
	public List<ProduitDto> findAll() {
		return produitDao.findAll()
				.parallelStream()
				.map(p->this.modelMapper.map(p,ProduitDto.class))
				.collect(Collectors.toList());
	}
	
	@Override
	public void add(ProduitDto pDto) {
		MultipartFile image = pDto.getImage();
		String contentType = null;
		String extension = null;
		if(image != null) {
			contentType = image.getContentType();
			String imageName = image.getOriginalFilename();
			int pointIndex = imageName.lastIndexOf('.');
			if(pointIndex != -1) {
				extension = imageName.substring(pointIndex+1);
			}
		}
		
		Produit produitEntity = this.modelMapper.map(pDto,Produit.class);
		produitEntity.setImgContentType(contentType);
		produitEntity.setExtension(extension);
		produitEntity = this.produitDao.save(produitEntity);
		
		pDto.setId(produitEntity.getId());
		
		if(image != null) {
			File f = new File(this.uploadFolderPath+File.separatorChar+produitEntity.getId()+'.'+extension);
			try {
				image.transferTo(f);
			} catch (IOException e) {
				log.error(e.getMessage(),e);
			}
		}
	}

	@Override
	public DownloadFileDto getImage(int prodId) throws IOException {
		Optional<Produit> prodOpt = this.produitDao.findById(prodId);
		if(prodOpt.isPresent()) {
			Produit produit = prodOpt.get();
			File f = new File(this.uploadFolderPath+File.separatorChar+prodId+'.'+produit.getExtension());
			InputStreamResource inputStreamResource = new InputStreamResource(new FileInputStream(f));
			return DownloadFileDto.builder()
					.inputStream(inputStreamResource)
					.length(f.length())
					.contentType(produit.getImgContentType())
					.build();
		}
		return null;
	}

}
